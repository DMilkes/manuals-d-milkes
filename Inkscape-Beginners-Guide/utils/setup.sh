#!/bin/bash

# Clone Inkscape Beginners' Guide if not already available,
# setup the environment and build it.
# Requires make, git, SSH keys set up with GitLab

# Determine the real location of the script (this is for running via symlink)
SCRIPT_PATH="${BASH_SOURCE[0]}"
while [ -h "$SCRIPT_PATH" ]; do
  # resolve $SCRIPT_PATH until the file is no longer a symlink
  SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_PATH" )" >/dev/null && pwd )"
  SCRIPT_PATH="$(readlink "$SCRIPT_PATH")"
  # if $SCRIPT_DIR was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  [[ $SCRIPT_PATH != /* ]] && SCRIPT_DIR="$SCRIPT_DIR/$SCRIPT_PATH"
done

SCRIPT_DIR="$( cd -P "$( dirname "$SCRIPT_PATH" )" >/dev/null && pwd )"
GIT_DIR="$SCRIPT_DIR"/../../.git

# Clone if needed
if [ ! -d "$GIT_DIR" ]; then
  git clone git@gitlab.com:inkscape/inkscape-docs/manuals.git
  cd manuals/Inkscape-Beginners-Guide/
else
  cd ..
fi

# Create virtual environment and activate it
python3 -m venv venv
source venv/bin/activate

# get necessary packages
pip install wheel
pip install sphinx latex sphinx-rtd-theme

# build documentation
make html
make latexpdf
